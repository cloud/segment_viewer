# segment_viewer

DNS PTR resolver for segments defined in `networks.yml`.

## Usage
```
docker run -it --rm \
       -v <ABSOLUTE_PATH>/networks.yml:/home/segment_viewer/home/segment_viewer/.segment_viewer/.networks.yml \
       -v segment_viewer:/home/segment_viewer/.segment_viewer:Z \
       registry.gitlab.ics.muni.cz:443/cloud/segment_viewer/segment_viewer
```
