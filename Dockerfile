FROM ruby:2.5.3-slim-stretch

LABEL Description="segment_viewer"

RUN apt-get update \
    && apt-get install -y build-essential \
    && rm -rf /var/lib/apt/lists/*


RUN useradd -ms /bin/bash segment_viewer
USER segment_viewer
WORKDIR /home/segment_viewer
RUN chmod 755 /home/segment_viewer

COPY Gemfile .
COPY Gemfile.lock .

RUN bundle install
RUN mkdir /home/segment_viewer/.segment_viewer

COPY segment_viewer.rb .

ENTRYPOINT /home/segment_viewer/segment_viewer.rb
